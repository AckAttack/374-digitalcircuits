/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Alec
 */
public class BitAdderComponentTest {
    private BitAdderComponent instance;  
    private TestTerminal input0, input1, input2, input3;    
        
    @Before
    public void setUp() {
        instance = new BitAdderComponent();      
        input0 = new TestTerminal();
        input1 = new TestTerminal();
        input2 = new TestTerminal();  
        input3 = new TestTerminal(); 
    }
    @Test
    public void testOutputs() { 
        Terminal output0 = instance.getOutput0();
        Terminal output1 = instance.getOutput1();
        assertTrue(output0 != null);
        assertTrue(output0.getClass() == OutputTerminal.class);
        assertTrue(output1 != null);
        assertTrue(output1.getClass() == OutputTerminal.class); 
    }         
    
    @Test
    public void testInput0() {
        instance.setInput0(input0);
        Terminal result = instance.getInput0();
        assertEquals("get input0 should return the Terminal set by setInput0", input0, result);
    }    
    
    @Test
    public void testInput1() {
        instance.setInput1(input1);
        Terminal result = instance.getInput1();
        assertEquals("get input1 should return the Terminal set by setInput1", input1, result);
    }  
    
    @Test
    public void testInput2() {
        instance.setInput2(input2);
        Terminal result = instance.getInput2();
        assertEquals("get input2 should return the Terminal set by setInput2", input2, result);
    }      
      
    @Test
    public void testNoInputs() {
        assertEquals("If the inputs aren't connected, the result should be zero", false, instance.getOutput0().getValue());
        assertEquals(instance.getOutput1().getValue(), false);
    }      
    
    @Test
    public void testInput0ValueChanged() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        input0.setValue(false);
        assertEquals("if the value of input0 changes then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());
        assertEquals(instance.getOutput1().getValue(), true);        
    }    
    
    @Test
    public void testInput0Changed() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        input3.setValue(false);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        instance.setInput0(input3);
        assertEquals("if input0 is connected to a different output terminal then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());        
        assertEquals(instance.getOutput1().getValue(), true);        
    }   
    
    @Test
    public void testInput0Cleared() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);        
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        instance.setInput0(null);
        assertEquals("if input0 is disconnected then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());        
        assertEquals(instance.getOutput1().getValue(), true);                
    }     
    
    @Test
    public void testInput1ValueChanged() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        input1.setValue(false);
        assertEquals("if the value of input1 changes then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());
        assertEquals(instance.getOutput1().getValue(), true);        
    }    
    
    @Test
    public void testInput1Changed() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        input3.setValue(false);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        instance.setInput1(input3);
        assertEquals("if input1 is connected to a different output terminal then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());        
        assertEquals(instance.getOutput1().getValue(), true);        
    }   
    
    @Test
    public void testInput1Cleared() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);        
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        instance.setInput1(null);
        assertEquals("if input1 is disconnected then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());        
        assertEquals(instance.getOutput1().getValue(), true);                
    }   

    @Test
    public void testInput2ValueChanged() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        input2.setValue(false);
        assertEquals("if the value of input2 changes then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());
        assertEquals(instance.getOutput1().getValue(), true);        
    }    
    
    @Test
    public void testInput2Changed() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        input3.setValue(false);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        instance.setInput2(input3);
        assertEquals("if input2 is connected to a different output terminal then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());        
        assertEquals(instance.getOutput1().getValue(), true);        
    }   
    
    @Test
    public void testInput2Cleared() {
        input0.setValue(true);
        input1.setValue(true);
        input2.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        instance.setInput2(input2);        
        assertEquals(instance.getOutput0().getValue(), true);
        assertEquals(instance.getOutput1().getValue(), true);
        
        instance.setInput2(null);
        assertEquals("if input2 is disconnected then the output of the Adder needs to be recomputed", false, instance.getOutput0().getValue());        
        assertEquals(instance.getOutput1().getValue(), true);                
    } 
}