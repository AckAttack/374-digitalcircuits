/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alec
 */
public class ORGateTest {
    private ORGate instance;  
    private TestTerminal input0, input1;    
    
    @Before
    public void setUp() {
        instance = new ORGate();      
        input0 = new TestTerminal();
        input1 = new TestTerminal();   
    }

    @Test
    public void testBothTrue() {
        input0.setValue(true);
        input1.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        assertEquals("true OR true should generate true", true, instance.getOutput().getValue());
    }
    
    @Test
    public void testTrueAndFalse() {
        input0.setValue(true);
        input1.setValue(false);
        instance.setInput0(input0);
        instance.setInput1(input1);
        assertEquals("true OR false should generate true", true, instance.getOutput().getValue());
    }
    
    @Test
    public void testFalseAndTrue() {
        input0.setValue(false);
        input1.setValue(true);
        instance.setInput0(input0);
        instance.setInput1(input1);
        assertEquals("false OR true should generate true", true, instance.getOutput().getValue());
    }
    
    @Test
    public void testBothFalse() {
        input0.setValue(false);
        input1.setValue(false);
        instance.setInput0(input0);
        instance.setInput1(input1);
        assertEquals("false OR false should generate false", false, instance.getOutput().getValue());
    }     
}
