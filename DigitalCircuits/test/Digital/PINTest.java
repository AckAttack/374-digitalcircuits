/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Aaron
 */
public class PINTest {
    private PIN instance;
    private TestTerminal input;
    
    @Before
    public void setUp() {
        instance = new PIN();      
        input = new TestTerminal();
    }
        
    @Test
    public void testTrue() {
        input.setValue(true);
        instance.setInput(input);
        assertEquals("True should generate true", true, instance.getOutput().getValue());
    }

            
    @Test
    public void testFalse() {
        input.setValue(false);
        instance.setInput(input);
        assertEquals("False should generate false", false, instance.getOutput().getValue());
    }
}
