/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Aaron
 */
public class NOTGateTest {
    private NOTGate instance;
    private TestTerminal input;
    
    @Before
    public void setUp() {
        instance = new NOTGate();      
        input = new TestTerminal();
    }
        
    @Test
    public void testTrue() {
        input.setValue(true);
        instance.setInput(input);
        assertEquals("True should generate false", false, instance.getOutput().getValue());
    }

            
    @Test
    public void testFalse() {
        input.setValue(false);
        instance.setInput(input);
        assertEquals("False should generate true", true, instance.getOutput().getValue());
    }
}
