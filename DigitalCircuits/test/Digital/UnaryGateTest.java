/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Alec
 */
public class UnaryGateTest {
    private UnaryGate instance;  
    private TestTerminal input0, input1;    
    
    @Before
    public void setUp() {
        instance = new PIN();      
        input0 = new TestTerminal();
        input1 = new TestTerminal(); 
    }
    
    @Test
    public void testOutput() { 
        Terminal output = instance.getOutput();
        assertTrue(output != null);
        assertTrue(output.getClass() == OutputTerminal.class);  
    }         
    
    @Test
    public void testInput() {
        instance.setInput(input0);
        Terminal result = instance.getInput();
        assertEquals("get input0 should return the Terminal set by setInput", input0, result);
    }    
      
    @Test
    public void testNoInputs() {
        assertEquals("if one of the inputs is not connected then that input value should be treated as false", false, instance.getOutput().getValue());
    }      
    
    @Test
    public void testInputValueChanged() {
        input0.setValue(true);
        instance.setInput(input0);
        assertEquals(instance.getOutput().getValue(), true);
        
        input0.setValue(false);
        assertEquals("if the value of input changes then the output of the gate needs to be recomputed", false, instance.getOutput().getValue());        
    }    
    
    @Test
    public void testInputChanged() {
        input0.setValue(true);
        input1.setValue(false);
        instance.setInput(input0);
        assertEquals(instance.getOutput().getValue(), true);
        
        instance.setInput(input1);
        assertEquals("if input is connected to a different output terminal then the output of the gate needs to be recomputed", false, instance.getOutput().getValue());        
    }   
    
    @Test
    public void testInputCleared() {
        input0.setValue(true);
        instance.setInput(input0);
        assertEquals(instance.getOutput().getValue(), true);
        
        instance.setInput(null);
        assertEquals("if input0 is disconnected then the output of the AND gate needs to be recomputed", false, instance.getOutput().getValue());        
    }     

}