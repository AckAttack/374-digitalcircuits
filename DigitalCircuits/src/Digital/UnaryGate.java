/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author Aaron
 */
public class UnaryGate extends javax.swing.JPanel implements PropertyChangeListener {
    private final Image image;
    private OutputTerminal output;
    private Terminal Input; 
    
    public final Terminal Output;
    
    public UnaryGate(String imagePath) {
        java.net.URL url = getClass().getResource(imagePath);
        image = new javax.swing.ImageIcon(url).getImage();
        this.setSize(image.getWidth(null), image.getHeight(null));
        
        output = new OutputTerminal();
        Output = output;
    }

    public Terminal getOutput() {
        return this.Output;
    }
    
    public Terminal getInput() {
        return this.Input;
    }
    
    public void setInput(Terminal t){
        this.Input = t;
        if (t != null){
            t.addPropertyChangeListener(this);
            this.output.setValue(this.Compute(this.Input.getValue()));
        } else {
            this.output.setValue(false);
        }
        
    }
    
    protected boolean Compute(boolean a){
        return a;
    }

    @Override
    public void paintComponent(java.awt.Graphics g){
        g.drawImage(image, 0, 0, null);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent e){
        this.output.setValue(this.Compute(this.getInput().getValue()));
    }   
}
