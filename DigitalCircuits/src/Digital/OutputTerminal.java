/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author Alec
 */
public class OutputTerminal implements Terminal {
    
    private boolean value;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport( this );
    
    @Override
    public boolean getValue(){
        return this.value;
    }
    public void setValue(boolean value){
        boolean oldValue = this.value;
        this.value = value;
        this.pcs.firePropertyChange( "Value", oldValue, value );
    }
    
    @Override
     public void addPropertyChangeListener( PropertyChangeListener pl ){
        this.pcs.addPropertyChangeListener( pl );
    }

    @Override
    public void removePropertyChangeListener( PropertyChangeListener pl ){
        this.pcs.removePropertyChangeListener( pl );
    }
}
