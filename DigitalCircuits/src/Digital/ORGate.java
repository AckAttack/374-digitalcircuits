/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

/**
 *
 * @author Aaron
 */
public class ORGate extends BinaryGate {
    
    public ORGate(){
        super("images/OR.gif");
    }

    @Override
    protected boolean Compute(boolean a, boolean b){
        return a || b;
    }
}
