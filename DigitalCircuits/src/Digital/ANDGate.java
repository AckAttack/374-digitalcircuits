/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

/**
 *
 * @author Aaron
 */

public class ANDGate extends BinaryGate {
    
    public ANDGate(){
        super("images/AND.gif");
    }

    @Override
    protected boolean Compute(boolean a, boolean b){
        return a && b;
    }
}