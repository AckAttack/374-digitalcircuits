/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;


import java.awt.Image;

/**
 *
 * @author Aaron
 */
public class BitAdderComponent extends javax.swing.JPanel {
    private BitAdderCircuit adder = new BitAdderCircuit();
    private final Image image;    
    
    public BitAdderComponent() {
        java.net.URL url = getClass().getResource("images/Adder.gif");
        image = new javax.swing.ImageIcon(url).getImage();
        this.setSize(image.getWidth(null), image.getHeight(null));
        
    }
        
    public void setInput0(Terminal input){
        adder.getPin1().setInput(input);
    }
    public Terminal getInput0(){
        return adder.getPin1().getInput();
    }
    
    public void setInput1(Terminal input){
        adder.getPin2().setInput(input);
    }
    public Terminal getInput1(){
        return adder.getPin2().getInput();
    }
    
    public void setInput2(Terminal input){
        adder.getPin3().setInput(input);
    }
    public Terminal getInput2(){
        return adder.getPin3().getInput();
    }            
    
    public Terminal getOutput0() {
        return adder.getPin4().getOutput();
    }
       
    public Terminal getOutput1() {
        return adder.getPin5().getOutput();
    } 
    
    @Override
    public void paintComponent(java.awt.Graphics g){
        g.drawImage(image, 0, 0, null);
    }
}
