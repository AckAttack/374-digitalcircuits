/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author Aaron
 */
public class BinaryGate extends javax.swing.JPanel implements PropertyChangeListener {
    private final Image image;
    private OutputTerminal output;
    private Terminal Input0, Input1; 
    
    public final Terminal Output;

    public BinaryGate(String imagePath) {
        java.net.URL url = getClass().getResource(imagePath);
        image = new javax.swing.ImageIcon(url).getImage();
        this.setSize(image.getWidth(null), image.getHeight(null));
        
        output = new OutputTerminal();
        Output = output;
    }

    public Terminal getOutput() {
        return this.Output;
    }
    
    public Terminal getInput0() {
        return this.Input0;
    }
    
    public void setInput0(Terminal t){
        this.Input0 = t;
        if (t != null){
            t.addPropertyChangeListener(this);
        }
        setInputHelper(this.getInput0(), this.getInput1());
    }
    
    public Terminal getInput1() {
        return this.Input1;
    }
    
    public void setInput1(Terminal t){
        this.Input1 = t;
        if (t != null){
            t.addPropertyChangeListener(this);
        }
        setInputHelper(this.getInput0(), this.getInput1());
    }
    
    private void setInputHelper(Terminal t0, Terminal t1){
        if (t0 == null){
            if (t1 == null){
                this.output.setValue(Compute(false, false));
            } else {
                this.output.setValue(Compute(false, t1.getValue()));
            }
        } else {
            if (t1 == null){
                this.output.setValue(Compute(t0.getValue(), false));
            } else {
                this.output.setValue(Compute(t0.getValue(), t1.getValue()));
            }
        }
    }
    
    protected boolean Compute(boolean a, boolean b){
        return a && b;
    }

    @Override
    public void paintComponent(java.awt.Graphics g){
        g.drawImage(image, 0, 0, null);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent e){
        this.output.setValue(this.Compute(this.getInput0().getValue(),
                                            this.getInput1().getValue()));
    }   
}
