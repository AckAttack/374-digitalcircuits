/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

/**
 *
 * @author Aaron
 */
public class NOTGate extends UnaryGate {
    
    public NOTGate(){
        super("images/NOT.gif");
    }

    @Override
    protected boolean Compute(boolean a){
        return !a;
    }
}
