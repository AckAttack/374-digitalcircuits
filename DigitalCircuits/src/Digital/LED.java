/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author Aaron
 */
public class LED extends javax.swing.JPanel implements PropertyChangeListener {
    private final Image imageLedOn, imageLedOff;
    private Image image;
    private Terminal Input; 
    
    
    public LED() {
        java.net.URL url = getClass().getResource("images/LED_off.gif");
        imageLedOff = new javax.swing.ImageIcon(url).getImage();
        this.setSize(imageLedOff.getWidth(null), imageLedOff.getHeight(null));
        image = imageLedOff;
        
        url = getClass().getResource("images/LED_on.gif");
        imageLedOn = new javax.swing.ImageIcon(url).getImage();
        this.setSize(imageLedOn.getWidth(null), imageLedOn.getHeight(null));        
    }
    
    public Terminal getInput() {
        return this.Input;
    }
    
    public void setInput(Terminal t){
        this.Input = t;
        if (t != null){
            t.addPropertyChangeListener(this);
            this.Repaint();
        } else {
            this.Repaint();
        }
        
    }
    
    private void Repaint(){
        if (this.Input != null){
            if (this.getInput().getValue() == true){
                this.image = this.imageLedOn;
                this.repaint();
            } else {
                this.image = this.imageLedOff;
                this.repaint();
            }
        } else {
            this.image = this.imageLedOff;
        }
    }
    
    @Override
    public void paintComponent(java.awt.Graphics g){
        g.drawImage(image, 0, 0, null);
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent e){
        this.Repaint();
    }   
}
