/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Digital;

/**
 *
 * @author Aaron
 */
public class XORGate extends BinaryGate {
    
    public XORGate(){
        super("images/XOR.gif");
    }

    @Override
    protected boolean Compute(boolean a, boolean b){
        return a ^ b;
    }
}